#!/bin/bash

# check throughput via experimentation on file sizes

dd if=/dev/zero of=small bs=1KB count=1
dd if=/dev/zero of=medium bs=1MB count=1
dd if=/dev/zero of=large bs=1MB count=1000

#./spidey.py -v p 9234        # run this in another command line before exectuing the script
                # this format works on the Linux cluster machines in the EG Lib

./tho.py -v -p 10 -r 5 0.0.0.0:9234/small 2>&1 > /dev/null | grep Average > small.txt
./tho.py -v -p 10 -r 5 0.0.0.0:9234/medium 2>&1 > /dev/null | grep Average > medium.txt
./tho.py -v -p 10 -r 5 0.0.0.0:9234/large 2>&1 > /dev/null | grep Average > large.txt

# format results

awk 'BEGIN {FS =" "}{print $6}' small.txt > small.dat
awk 'BEGIN {FS =" "}{print $6}' medium.txt > medium.dat
awk 'BEGIN {FS =" "}{print $6}' large.txt > large.dat